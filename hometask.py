workdays = "ПН", "ВТ", "СР", "ЧТ", "ПТ"
dayoff = "СБ", "НД"

while True:
    current_day = input("введіть день: ")
    if not current_day:
        break
    print("робочій день" if current_day in workdays else "вихідний день" if current_day in dayoff else "не існує" )